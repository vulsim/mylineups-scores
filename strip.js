var util = require('util');
var fs = require('fs');
var path  = require('path');
var src = require('./schedule.json');
var dst = {};

var tac = {
	"ARZ": "Arizona Cardinals",
	"ARI": "Arizona Cardinals",
	"ATL": "Atlanta Falcons",
	"BAL": "Baltimore Ravens",
	"BLT": "Baltimore Ravens",
	"BUF": "Buffalo Bills",
	"CAR": "Carolina Panthers",
	"CHI": "Chicago Bears",
	"CIN": "Cincinnati Bengals",
	"CLE": "Cleveland Browns",
	"CLV": "Cleveland Browns",
	"DAL": "Dallas Cowboys",
	"DEN": "Denver Broncos",
	"DET": "Detroit Lions",
	"GB": "Green Bay Packers",
	"HOU": "Houston Texans",
	"HST": "Houston Texans",
	"IND": "Indianapolis Colts",
	"JAC": "Jacksonville Jaguars",
	"JAX": "Jacksonville Jaguars",
	"KC": "Kansas City Chiefs",
	"MIA": "Miami Dolphins",
	"MIN": "Minnesota Vikings",
	"NE": "New England Patriots",
	"NO": "New Orleans Saints",
	"NYG": "New York Giants",
	"NYJ": "New York Jets",
	"OAK": "Oakland Raiders",
	"PHI": "Philadelphia Eagles",
	"PIT": "Pittsburgh Steelers",
	"SL": "St. Louis Rams",
	"STL": "St. Louis Rams",
	"SD": "San Diego Chargers",
	"SF": "San Francisco 49ers",
	"SEA": "Seattle Seahawks",
	"TB": "Tampa Bay Buccaneers",
	"TEN": "Tennessee Titans",
	"WAS": "Washington Redskins"
};

var mkdirSync = function (path) {
	try {
		fs.mkdirSync(path);
	} catch (e) {
		if (e.code != 'EEXIST') {
			throw e;
		}
	}
};

var writeObjectSync = function (path, obj) {
	try {
		fs.writeFileSync(path, JSON.stringify(obj, null, 4));    
    } catch (e) {
    
    }
};

for (var index in src.games) {

	if (dst[src.games[index][1].year] == undefined) {
		dst[src.games[index][1].year] = [];
	}	

	var srcObj = src.games[index][1];
	var timeComp = srcObj.time.split(":");
	var utcTime = (parseInt(timeComp[0]) + 12) + ":" + timeComp[1];
	var dstObj = {
		"eid": srcObj.eid,
		"gamekey": srcObj.gamekey,
		"week": srcObj.week,
		"home_short": srcObj.home,
		"home_full": (tac[srcObj.home] != undefined) ? tac[srcObj.home] : srcObj.home,
		"away_short": srcObj.away,
		"away_full": (tac[srcObj.away] != undefined) ? tac[srcObj.away] : srcObj.away,
		"season_type": srcObj.season_type,
		"date": util.format("%s-%s-%s %s", srcObj.year, (srcObj.month.toString().length > 1) ? srcObj.month : "0" + srcObj.month, (srcObj.day.toString().length > 1) ? srcObj.day : "0" + srcObj.day, utcTime)
	};

	dst[src.games[index][1].year].push(dstObj);	
}

mkdirSync("./schedule");

for (var year in dst) {
	mkdirSync("./schedule/" + year);
	writeObjectSync("./schedule/" + year + "/games.json", {
		"games": dst[year],
		"time": src.time
	});
}
