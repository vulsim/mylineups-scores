var util = require('util');
var fs = require('fs');
var path  = require('path');
var src1 = require('./schedule.json');
var src2 = require('./schedule2.json');
var dst = {};

var tac = {
	"ARZ": "Arizona Cardinals",
	"ARI": "Arizona Cardinals",
	"ATL": "Atlanta Falcons",
	"BAL": "Baltimore Ravens",
	"BLT": "Baltimore Ravens",
	"BUF": "Buffalo Bills",
	"CAR": "Carolina Panthers",
	"CHI": "Chicago Bears",
	"CIN": "Cincinnati Bengals",
	"CLE": "Cleveland Browns",
	"CLV": "Cleveland Browns",
	"DAL": "Dallas Cowboys",
	"DEN": "Denver Broncos",
	"DET": "Detroit Lions",
	"GB": "Green Bay Packers",
	"HOU": "Houston Texans",
	"HST": "Houston Texans",
	"IND": "Indianapolis Colts",
	"JAC": "Jacksonville Jaguars",
	"JAX": "Jacksonville Jaguars",
	"KC": "Kansas City Chiefs",
	"MIA": "Miami Dolphins",
	"MIN": "Minnesota Vikings",
	"NE": "New England Patriots",
	"NO": "New Orleans Saints",
	"NYG": "New York Giants",
	"NYJ": "New York Jets",
	"OAK": "Oakland Raiders",
	"PHI": "Philadelphia Eagles",
	"PIT": "Pittsburgh Steelers",
	"SL": "St. Louis Rams",
	"STL": "St. Louis Rams",
	"SD": "San Diego Chargers",
	"SF": "San Francisco 49ers",
	"SEA": "Seattle Seahawks",
	"TB": "Tampa Bay Buccaneers",
	"TEN": "Tennessee Titans",
	"WAS": "Washington Redskins"
};

var mkdirSync = function (path) {
	try {
		fs.mkdirSync(path);
	} catch (e) {
		if (e.code != 'EEXIST') {
			throw e;
		}
	}
};

var writeObjectSync = function (path, obj) {
	try {
		fs.writeFileSync(path, JSON.stringify(obj, null, 4));    
    } catch (e) {
    
    }
};

for (var index in src2.Schedule) {

	var obj1 = src2.Schedule[index];
	var dateComp = obj1.gameDate.split("-");
	var timeComp = obj1.gameTimeET.split(" ");
	var hoursMinutesComp = timeComp[0].split(":");

	var year = dateComp[0];
	var month = dateComp[1];
	var day = dateComp[2];
	var hoursAmPm = parseInt(hoursMinutesComp[0]);
	var hours = (timeComp[1] == "PM") ? (parseInt(hoursAmPm) + 12).toString() : hoursAmPm;
	var minutes = hoursMinutesComp[1];

	var yearInt1 = parseInt(year);
	var monthInt1 = parseInt(month);
	var dayInt1 = parseInt(day);
	var hoursAmPmInt1 = parseInt(hoursAmPm);
	var minutesInt1 = parseInt(minutes);

	for (var index2 in src1.games) {
		var obj2 = src1.games[index2][1];
		var timeComp = obj2.time.split(":");

		var yearInt2 = parseInt(obj2.year);
		var monthInt2 = parseInt(obj2.month);
		var dayInt2 = parseInt(obj2.day);
		var hoursAmPmInt2 = parseInt(timeComp[0]);
		var minutesInt2 = parseInt(timeComp[1]);

		if (yearInt1 == yearInt2 && monthInt1 == monthInt2 && dayInt1 == dayInt2 && hoursAmPmInt1 == hoursAmPmInt2 && minutesInt1 == minutesInt2) {
			obj1.eid = obj2.eid;
			obj1.season_type = obj2.season_type;
			obj1.gamekey = obj2.gamekey;
			break;
		}
	}
	
	if (dst[year] == undefined) {
		dst[year] = [];
	}	
	
	var dstObj = {
		"eid": obj1.eid,
		"gamekey": obj1.gamekey,
		"week": obj1.gameWeek,
		"home_short": obj1.homeTeam,
		"home_full": (tac[obj1.homeTeam] != undefined) ? tac[obj1.homeTeam] : obj1.homeTeam,
		"away_short": obj1.awayTeam,
		"away_full": (tac[obj1.awayTeam] != undefined) ? tac[obj1.awayTeam] : obj1.awayTeam,
		"season_type": obj1.season_type,
		"date": util.format("%s-%s-%sT%s:%s:00-05", year, (month.toString().length > 1) ? month : "0" + month, (day.toString().length > 1) ? day : "0" + day, hours, minutes)
	};

	dst[year].push(dstObj);	
}

mkdirSync("./schedule");

for (var year in dst) {
	mkdirSync("./schedule/" + year);
	writeObjectSync("./schedule/" + year + "/games.json", {
		"games": dst[year],
		"time": Date()
	});
}
